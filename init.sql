CREATE TABLE IF NOT EXISTS search_index (
    id varchar(21) PRIMARY KEY,
    cropped_picture varchar(255),
    keywords varchar(500),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    expires_at TIMESTAMP
);

CREATE UNIQUE INDEX uq_search_index_cropped_picture ON search_index (cropped_picture);

CREATE INDEX idx_search_index_keywords ON search_index USING gin(to_tsvector('english', keywords));
