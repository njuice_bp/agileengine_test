FROM python:3.6

ENV PYTHONPATH  .

ADD . /opt/app

WORKDIR /opt/app

RUN pip install -r requirements.txt

EXPOSE 8088

CMD python -m aiohttp.web -H localhost -P 8088 agileengine_test.app:run
