import os

# Agile Engine creds
AGILEENGINE_API_KEY = '23567b218376f79d9415'
AGILEENGINE_ENDPOINT = 'http://interview.agileengine.com'

# DB
DB_USER = os.environ.get("DB_USER", "agileengine")
DB_NAME = os.environ.get("DB_NAME", "agileengine")
DB_HOST = os.environ.get("DB_HOST", "localhost")
DB_PORT = int(os.environ.get("DB_PORT", 5432))
DB_PASSWORD = os.environ.get("DB_PASSWORD", "agileengine")
DB = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

# SEARCH INDEX
SEARCH_INDEX_TTL = 60 * 5  # 5 min.
FIELDS_TO_INDEX = ['author', 'camera', 'tags']
