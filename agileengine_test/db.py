from aiopg.sa import create_engine

from agileengine_test import settings

engine = None


async def get_engine():
    global engine
    if not engine:
        engine = await create_engine(
            user=settings.DB_USER,
            database=settings.DB_NAME,
            host=settings.DB_HOST,
            password=settings.DB_PASSWORD)
    return engine
