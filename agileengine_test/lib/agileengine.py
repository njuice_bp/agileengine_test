from loguru import logger
import urllib.parse as urlparse


class AgileEngineClient:

    def __init__(self, settings, httpClient=None):
        self.token = None

        self.apiKey = settings.AGILEENGINE_API_KEY
        self.endpoint = settings.AGILEENGINE_ENDPOINT
        self.httpClient = httpClient

    async def __refresh_token(self):
        logger.info("Refreshing token")
        url = urlparse.urljoin(self.endpoint, '/auth')
        logger.info(url)
        resp = await self.httpClient.post(url, json={'apiKey': self.apiKey})
        data = await resp.json()
        self.token = data.get('token')
        logger.info(f"Token {self.token}")

    async def __authorized_request(self, url, params=None):
        params = params or {}

        if not self.token:
            await self.__refresh_token()

        if params:
            url = f"{url}?{urlparse.urlencode(params)}"

        resp = await self.httpClient.get(url, headers={'Authorization': f'{self.token}'})
        status = resp.status

        if status == 401:
            # NOTE:
            # Here we should add exponential backoff and raise an exception
            # if too many unsuccessful refresh token attempts
            await self.__refresh_token()
            return await self.__authorized_request(url, params)

        data = await resp.json()
        return data

    async def retrieve_image_data(self, id):
        url = urlparse.urljoin(self.endpoint, f'/images/{id}')
        data = await self.__authorized_request(url)
        return data

    async def list_images(self, page=1):
        url = urlparse.urljoin(self.endpoint, '/images')
        params = {'page': page}
        data = await self.__authorized_request(url, params)
        return data.get('pictures'), data.get('hasMore')
