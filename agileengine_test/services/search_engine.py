import aiohttp
from loguru import logger
from datetime import timedelta

from agileengine_test import settings
from agileengine_test.lib.helpers import now
from agileengine_test.dal import search_index
from agileengine_test.lib.agileengine import AgileEngineClient


__all__ = ['rebuild_index', 'find_all']

agileengine = AgileEngineClient(
        settings, httpClient=aiohttp.ClientSession())


def fetch_image_keywords(image):
    keywords = []
    for field in settings.FIELDS_TO_INDEX:
        if not image.get(field):
            continue
        keywords.append(image.get(field))
    return '|'.join(keywords)


async def rebuild_index():
    page = 1
    has_more = True
    while has_more:
        (images, has_more) = await agileengine.list_images(page)
        for image in images:
            image_data = await agileengine.retrieve_image_data(image.get('id'))
            await search_index.update({
                'id': image['id'],
                'cropped_picture': image['cropped_picture'],
                'keywords': fetch_image_keywords(image_data),
                'created_at': now(),
                'expires_at': now() + timedelta(seconds=settings.SEARCH_INDEX_TTL)
            })
        page += 1
        logger.info(f'Search index update for {len(images)} image(s). Has more: {has_more}')
    await search_index.clean_expired()
    logger.info('Expired items was removed')

async def find_all(search_str):
    logger.debug('find_all')
    matches = await search_index.find_all(search_str)
    return matches
