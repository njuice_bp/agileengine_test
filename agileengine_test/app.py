import asyncio

from aiohttp import web
from loguru import logger

from agileengine_test import settings
from agileengine_test.lib.helpers import now
from agileengine_test.services import search_engine
from agileengine_test.views.search import ListSearchView


async def update_search_index_task():
    while True:
        try:
            logger.info("Start")
            start_time = now()
            await search_engine.rebuild_index()
            finish_time = now()
            logger.info(f"Finished ({finish_time - start_time})")
        except Exception as e:
            logger.error(f"Can't rebuild search index. Reason: {e}")
        finally:
            sleep = settings.SEARCH_INDEX_TTL//2
            if sleep > 0:
                logger.info(f"Next index update after {sleep} sec.")
                await asyncio.sleep(sleep)


async def start_background_tasks(app):
    app['update_search_index'] = app.loop.create_task(update_search_index_task())


async def cleanup_background_tasks(app):
    app['update_search_index'].cancel()
    await app['update_search_index']


async def run(args):
    app = web.Application()
    app.router.add_view('/search/{search_term}', ListSearchView)

    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
    return app
