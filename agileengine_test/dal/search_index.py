from loguru import logger
from sqlalchemy import text

from agileengine_test.db import get_engine
from agileengine_test.lib.helpers import now

# NOTE: Another one possible solution how to organize DB is to have 2 tables - ImagesCache and SearchIndex.
# But it is more complex and doesn't work for distributed databases that doesn't support joins for example

async def update(data):
    db = await get_engine()
    async with db.acquire() as conn:
        stmt = text("""INSERT INTO search_index(id, cropped_picture, keywords, created_at, expires_at) 
            VALUES (:id, :cropped_picture, :keywords, :created_at, :expires_at) 
            ON CONFLICT (id) 
            DO UPDATE SET keywords=:keywords, expires_at=:expires_at;""")

        await conn.execute(stmt.bindparams(**data))


async def clean_expired():
    db = await get_engine()
    async with db.acquire() as conn:
        stmt = text("DELETE FROM search_index WHERE expires_at<=:expires_at;")
        await conn.execute(stmt.bindparams(expires_at=now()))


async def find_all(search_str):
    db = await get_engine()
    async with db.acquire() as conn:
        stmt = text("""SELECT * FROM search_index WHERE expires_at > :now AND 
            to_tsvector(keywords) @@ to_tsquery(:search_str);""")
        res = await conn.execute(stmt.bindparams(now=now(), search_str=search_str))
        rows = await res.fetchall()
        # Unfortunately, I didn't found another solution to normalize data here
        return [{'id': row[0], 'cropped_picture': row[1]} for row in rows]
