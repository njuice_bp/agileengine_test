from aiohttp import web
from agileengine_test.services import search_engine


class ListSearchView(web.View):

    async def get(self):
        search_term = self.request.match_info.get('search_term')
        resp = await search_engine.find_all(search_term)
        return web.json_response(resp)
