
# Local deployment

- Install Docker and Docker Compose if need
- Clone current repository to local machine
- Run a service


```shell
 :~$ cd agileengine_test
 :~$ sudo docker-compose up 
```		
 

- Check container logs to ensure that search index was built
- Check http://localhost:8088/search/{searchTerm}
